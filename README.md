# [joe-alm](https://joe-alm.herokuapp.com)
a sophisticated [GraphQL](http://graphql.org/) endpoint for [GitLab](https://gitlab.com) for creating an automatic Kanban board.

# CLI
using [joe-alm-cli](https://gitlab.com/Schniz/joe-alm-cli)

# How does it work

Its all about *issues*.

- **Backlog** issues are issues that are not in a *milestone*.
- **To do** issues are issues that are in a *milestone* but they does not have a **closing** *merge request*.
- **WIP** issues are issues that are in a *milestone*, have a **closing** `work in progress` *merge request*.
- **Review** issues are issues that are in a *milestone*, have a **closing** *merge request* that is not `work in progress`.
- **Done** issues are closed issues.

# [Test project](https://gitlab.com/Schniz/joe-alm-test-project)
