import express from 'express';
import bodyParser from 'body-parser';
import schema from './graphql/schema';
import graphqlHTTP from 'express-graphql';
import createDebugger from './debug';
import cors from 'cors';

const { debug } = createDebugger('webserver');
const PORT = process.env.APP_PORT || process.env.PORT || 8081;
const app = express();

app.use((req, res, next) => {
  const time = new Date().getTime();
  const oldEnd = res.end.bind(res);
  res.end = (...args) => {
    oldEnd(...args);
    console.log(`it took ${new Date().getTime() - time} millis`);
  };

  next();
});

app.use(cors());

app.use(bodyParser.json({ extended: true }));
app.use('/graphql', graphqlHTTP(request => ({
  schema,
  rootValue: {
    token: request.headers.token || request.query.token,
  },
  graphiql: true,
})));

app.get('/', (req, res) => {
  res.redirect('/graphql');
});

app.listen(PORT, () => {
  debug(`Listening on ${PORT}. Ctrl-C to stop.`);
});
