import createDebugger from './debug';

const { debug } = createDebugger('configuration');
const env = process.env;

const configuration = {
  gitlabHost: env.GITLAB_HOST || 'http://192.168.99.100',
};

debug('Using configuration:', configuration);

module.exports = configuration;
