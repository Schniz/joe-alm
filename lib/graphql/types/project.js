import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql';
import gitlab from '../gitlab';
import MilestoneType from './milestone';
import IssueType from './issue';
import OwnerType from './owner';

export default new GraphQLObjectType({
  name: 'Project',
  fields: () => ({
    name: { type: GraphQLString },
    id: { type: GraphQLID },
    members: {
      type: new GraphQLList(OwnerType),
      resolve: (it, {}, { rootValue: { token } }) => (
        gitlab(token, 'projects', it.id, 'members')
      ),
    },
    backlog: {
      type: new GraphQLList(IssueType),
      resolve: (it, {}, { rootValue: { token } }) => (
        gitlab(token, 'projects', it.id, 'issues', { milestone_id: 'No Milestone' }).then(e => e || []).then(e => (
          e.filter(e => !e.milestone)
        ))
      ),
    },
    milestoneById: {
      type: MilestoneType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (it, { id }, { rootValue: { token } }) => (
        gitlab(token, 'projects', it.id, 'milestones', id).then(x => ({
          ...x,
          projectId: it.id
        }))
      ),
    },
    milestones: {
      type: new GraphQLList(MilestoneType),
      resolve: (it, {}, { rootValue: { token } }) => (
        gitlab(token, 'projects', it.id, 'milestones').then(e => e || []).then(e => e.map(x => ({
          ...x,
          projectId: it.id
        })))
      ),
    }
  }),
});
