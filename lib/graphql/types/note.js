import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
  GraphQLID,
} from 'graphql';

import OwnerType from './owner';

export default new GraphQLObjectType({
  name: 'Note',
  fields: () => ({
    id: { type: GraphQLID },
    body: { type: GraphQLString },
    author: { type: OwnerType },
    downvote: { type: GraphQLBoolean },
    system: { type: GraphQLBoolean },
    upvote: { type: GraphQLBoolean },
    attachment: { type: GraphQLString },
    createdAt: { type: GraphQLString },
  }),
});
