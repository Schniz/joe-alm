import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql';
import Promise from 'bluebird';
import IssueType from './issue';
import gitlab from '../gitlab';
import createDebugger from '../../debug';
import { curry, map, flatten } from 'ramda';

const { debug } = createDebugger('graphql:milestone');

/**
 * Returns the closes issues by a merge request
 *
 * @param {string} token the access token
 * @param {string} projectId to query
 * @param {string} mergeRequestId to query
 * @returns {Promise} with an {array} of issues as the result
 */
function getClosesIssues(token, projectId, mergeRequestId) {
  return gitlab(token, 'projects', projectId, 'merge_requests', mergeRequestId, 'closes_issues').then(e => e || []).then(map(issue => {
    let newIssue = {
      ...issue,
      closingMergeRequest: mergeRequestId,
    };
    return newIssue;
  }));
}

/**
 * Returns the closes issues by a merge request
 * Curry function
 *
 * @param {string} token the access token
 * @param {object} mergeRequest a merge request
 * @returns {Promise} with an {array} of issues as the result
 */
const getClosesIssuesForMergeRequest = curry((token, mergeRequest) => {
  return getClosesIssues(token, mergeRequest.project_id,  mergeRequest.id);
});

/**
 * creates an issue map with the keys as the kanban columns
 *
 * @param {object} data an object containing
 * @param {array} data.issues an array of issues
 * @param {array} data.waitingForReview an array of issues that are waiting for review
 * @param {array} data.wipIssues an array of issues that are in progress
 * @returns {object} where the key is the kanban column and the value is an array of issues.
 */
function createIssueMap({ issues, waitingForReview, wipIssues }) {
  let issuesMap = issues.reduce((acc, issue) => {
    let key;
    let issueId = issue.id;

    if (issue.state === 'closed') {
      key = 'done';
    } else if (waitingForReview.indexOf(issueId) !== -1) {
      key = 'review';
    } else if (wipIssues.indexOf(issueId) !== -1) {
      key = 'wip';
    } else {
      key = 'todo';
    }

    return { ...acc, [key]: acc[key].concat(issue) };
  }, { done: [], review: [], wip: [], todo: [] });
  return issuesMap;
}

/**
 * Get issues by work status
 *
 * @param {object} data an object
 * @param {string} data.status one of ['done','review','wip','todo']
 * @param {string} data.token the access token
 * @param {string} data.projectId to query
 * @param {string} data.title milestone title
 * @returns {undefined}
 */
function getIssues({ status, token, projectId, title }) {
  const mrs = gitlab(token, 'projects', projectId, 'merge_requests').then(e => e || []);
  return Promise.all([
    gitlab(token, 'projects', projectId, 'issues', { milestone: title }),
    mrs,
    Promise.all(mrs.then(map(getClosesIssuesForMergeRequest(token)))).then(flatten)
  ]).spread((issues, mergeRequests, workInProgresses) => {
    let wipIssues = workInProgresses.map(issue => issue.id);
    let waitingForReview = mergeRequests.filter(mergeRequest => {
      return ['active', 'opened', 'reopened'].indexOf(mergeRequest.state) !== -1 && !mergeRequest.work_in_progress;
    }).map(mergeRequest => {
      return workInProgresses.find(e => e.closingMergeRequest === mergeRequest.id);
    }).map(e => e.id);

    let issuesMap = createIssueMap({ issues, wipIssues, waitingForReview });
    debug('found issue map:', issuesMap);
    return issuesMap[status];
  });
}

function issuer(type, description) {
  return {
    type: new GraphQLList(IssueType),
    resolve: (it, {}, { rootValue: { token } }) => (
      getIssues({ status: type, token, projectId: it.projectId, title: it.title })
    ),
    description,
  };
}

export default new GraphQLObjectType({
  name: 'Milestone',
  description: 'Represents a `sprint` in the graph data',
  fields: () => ({
    id: { type: GraphQLID },
    title: { type: GraphQLString },
    todo: issuer('todo', 'Issues the team need to do'),
    wip: issuer('wip', 'Issues that are in progress'),
    done: issuer('done', 'Issues that are already done'),
    review: issuer('review', 'Issues that are in the review stage'),
    issuesForType: {
      type: new GraphQLList(IssueType),
      args: {
        type: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve: (it, { type }, { rootValue: { token } }) => {
        return getIssues({ status: type, token, projectId: it.projectId, title: it.title });
      },
    }
  }),
});
