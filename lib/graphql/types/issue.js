import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
} from 'graphql';
import OwnerType from './owner';
import NoteType from './note';
import gitlab from '../gitlab';

export default new GraphQLObjectType({
  name: 'Issue',
  fields: () => ({
    id: {
      type: GraphQLID,
      description: 'The global ID of the issue in gitlab',
    },
    iid: {
      type: GraphQLInt,
      description: 'The ID of the issue specific to the project',
      resolve: it => it.iid
    },
    title: {
      type: GraphQLString,
      description: 'The issue title',
    },
    author: {
      type: OwnerType,
      description: 'The `author` of this issue',
    },
    assignee: {
      type: OwnerType,
      description: 'The `assignee` of this issue',
    },
    createdAt: {
      type: GraphQLString,
    },
    updatedAt: {
      type: GraphQLString,
    },
    notes: {
      type: new GraphQLList(NoteType),
      description: 'The notes posted to this issue',
      resolve: (it, {}, { rootValue: { token } }) => (
        gitlab(token, 'projects', it.project_id, 'issues', it.id, 'notes')
      )
    },
  }),
});
