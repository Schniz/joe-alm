import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
} from 'graphql';

export default new GraphQLObjectType({
  name: 'Owner',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    username: { type: GraphQLString },
    avatar: { type: GraphQLString, resolve: it => it.avatar_url },
    url: { type: GraphQLString, resolve: it => it.web_url },
  }),
});
