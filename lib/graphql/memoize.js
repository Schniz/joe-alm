import Promise from 'bluebird';
import LRU from 'lru-cache';
import createDebugger from '../debug';
const { debug } = createDebugger('memoize');
const options = {
  maxAge: 1000 * 20,
};

let memoizationTable = {};
export default function memoize(func) {
  const funcString = func.toString();
  memoizationTable[funcString] = LRU(options);
  return function () {
    const argsString = JSON.stringify(arguments);
    if (!memoizationTable[funcString].get(argsString)) {
      debug('cache miss');
      memoizationTable[funcString].set(argsString, func.apply(this, arguments));
    } else {
      debug('cache hit!');
    }

    return memoizationTable[funcString].get(argsString);
  };
}

function getFromMemo(func, args) {
  const result = memoizationTable[func.toString()].get(JSON.stringify(args));
  if (result) {
    return Promise.resolve(result);
  } else {
    return Promise.reject();
  }
}

function storeInMemo(func, args) {
  return data => {
    memoizationTable[func.toString()].set(JSON.stringify(args), data);
    return data;
  };
}

function callAndStore(func, args) {
  return Promise.resolve().then(() => {
    const result = func(...args);
    if (result.then) {
      return result.then(storeInMemo(func, args));
    } else {
      return storeInMemo(result);
    }
  });
}

export function mem2(func) {
  const funcString = func.toString();
  memoizationTable[funcString] = LRU(options);
  return (...args) => {
    const get = getFromMemo(func, args);
    const call = callAndStore(func, args);
    return Promise.any([get, call]);
  };
}
