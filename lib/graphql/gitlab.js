import request from 'superagent';
import memoize from './memoize';
import Promise from 'bluebird';
import { gitlabHost } from '../config';
import createDebugger from '../debug';

const { debug } = createDebugger('gitlab-api');
const pageRegex = /page=(\d+)&/;

const extractPathAndObj = (args) => {
  let realArgs = args;
  let count = args.length;
  let obj = args[count - 1];
  if (typeof obj === 'object' && !Array.isArray(args[count - 1])) {
    realArgs = args.slice(0, count - 1);
  } else {
    obj = {};
  }

  return [realArgs, { page: 1, per_page: 100, ...obj }];
};

const pRequest = memoize((token, path, obj) => new Promise((resolve, reject) => {
  let objString = Object.keys(obj).reduce((acc,e) => `${acc}&${encodeURIComponent(e)}=${encodeURIComponent(obj[e])}`, '');
  let url = `${gitlabHost}/api/v3/${path.map(encodeURIComponent).join('/')}?private_token=${token}${objString}`;
  let time = new Date().getTime();
  request(
    url,
    (err, res) =>  err ? reject(err) : (
      resolve(res) ||
      debug(`calling url ${url} took ${new Date().getTime() - time}ms`) && false
    )
  );
}));

export function query(token, ...args) {
  const [path, obj] = extractPathAndObj(args);
  return pRequest(token, path, obj).then(res => {
    if (!res.links || !res.links.last) return res.body;
    const lastPage = parseInt(pageRegex.exec(res.links.last)[1]);
    if (obj.page === lastPage) return res.body;
    return Promise.all(Array.from({ length: lastPage - obj.page + 1 }, (e, i) => (
      i === 0 ? Promise.resolve(res.body) : pRequest(token, path, { ...obj, page: i + obj.page })
    ))).reduce((acc, e) => acc.concat(e.body));
  });
}

export default memoize(query);
