import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
} from 'graphql';
import g from './gitlab';
import ProjectType from './types/project';

export default new GraphQLObjectType({
  name: 'Query',
  description: 'All of our queries...',
  fields: () => ({
    project: {
      description: 'Query a project by `path`. for instance: `root/test-project`',
      type: ProjectType,
      args: {
        path: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (it, { path }, { rootValue: { token } }) => {
        return g(token, 'projects', path);
      },
    },
    projects: {
      description: 'Query all the projects.',
      type: new GraphQLList(ProjectType),
      resolve: (it, { path }, { rootValue: { token } }) => {
        return g(token, 'projects');
      },
    },
  }),
});
