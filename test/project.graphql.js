import schema from '../lib/graphql/schema';
import { graphql } from 'graphql';
import createDebugger from '../lib/debug';
import { curry } from 'ramda';
import token from './token';

const { debug } = createDebugger('test:project.graphql');

const ctx = {
  token,
};

const exec = curry((query, params) => graphql(schema, query, ctx, params).then(debug).then(d => {
  if (d.errors) throw d.errors[0].originalError;
  return d;
}));

const projectQuery = exec(`
  query fetchProject($path: String!, $type: String!) {
    project(path: $path) {
      name,
      members {
        username,
      }
      backlog { id, title, notes { body, system } },
      milestones {
        title,
        todo { title },
        wip { title },
        review { title },
        done { title },
        issuesForType(type: $type) { title },
      }
    }
  }
`);

describe('gitlab api to graphql', function () {
  this.timeout(15000);

  it('single project', (done) => {
    projectQuery({ path: 'Schniz/joe-alm-test-project', type: 'wip' }).then(debug).then(e => {
      e.data.project.name.should.equal('joe-alm-test-project');
      e.data.project.backlog.should.have.length(2);
      e.data.project.milestones[0].todo.should.have.length(1);
      e.data.project.milestones[0].wip.should.have.length(1);
      e.data.project.milestones[0].review.should.have.length(1);
      e.data.project.milestones[0].done.should.have.length(1);
      e.data.project.milestones[0].issuesForType.should.deep.equal(e.data.project.milestones[0].wip);
      return e.data.project.name;
    }).should.notify(done);
  });
});
