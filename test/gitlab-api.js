import gitlab from '../lib/graphql/gitlab';
import token from './token';

describe('gitlab', function () {
  this.timeout(10000);

  it('should query some projects', done => {
    gitlab(token, 'projects').then(([result]) => {
      result.owner.name.should.be.a('string');
      done();
    }).catch(done);
  });
});
