import memoize from '../lib/graphql/memoize';

describe('memoization', function () {
  it('should memoize long running functions', function (done) {
    this.timeout(1600);

    const longRunning = () => new Promise(resolve => {
      setTimeout(() => resolve('hey'), 1500);
    });

    const memoized = memoize(longRunning);
    memoized().then(memoized()).then(memoized()).should.become('hey').and.notify(done);
  });

  it('should update async', function (done) {
    this.timeout(3100);

    let i = 1;
    const longRunning = () => new Promise(resolve => {
      setTimeout(() => resolve(i++), 1500);
    });
    const memoized = memoize(longRunning);

    memoized().then(memoized()).should.become(1).then(() => {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve(memoized());
        }, 1510);
      });
    }).should.become(2).and.notify(done);
  });
});
